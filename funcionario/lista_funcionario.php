<?php

if (!isset($_SESSION['logado'])) {
  header('Location: login.php');
}
?>

<div class="container">
  <h2>Funcionario</h2>
  <a class="btn btn-info" href="funcionario.php?acao=novo">Novo</a>
  <?php if (count($registros)==0): ?>
    <p>Nenhum registro encontrado.</p>
  <?php else: ?>
    <table class="table table-hover table-stripped">
      <thead>
          <th>#</th>
          <th>Nome</th>
          <th>Função</th>
          <th>CPF</th>
          <th>Salario</th>
          <th>Ações</th>
      </thead>
      <tbody>
        <?php foreach ($registros as $linha): ?>
          <tr>
            <td><?php echo $linha['id']; ?></td>
            <td><?php echo $linha['nome']; ?></td>
            <td><?php echo $linha['funcao']; ?></td>
            <td><?php echo $linha['cpf']; ?></td>
            <td><?php echo $linha['salario']; ?></td>
            <td>
                <a class="btn btn-warning btn-sm" href="funcionario.php?acao=buscar&id=<?php echo $linha['id']; ?>">Editar</a>
                <a class="btn btn-danger btn-sm" href="funcionario.php?acao=excluir&id=<?php echo $linha['id']; ?>">Excluir</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  <?php endif; ?>
</div>