<?php

if (!isset($_SESSION['logado'])) {
  header('Location: login.php');
}
?>

<?php
    if(isset($registro)) $acao = "funcionario.php?acao=atualizar&id=".$registro['id'];
    else $acao = "funcionario.php?acao=gravar";
 ?>
<div class="container">
  <form class="" action="<?php echo $acao; ?>" method="post">
    <div class="from-group">
      <label for="nome">Nome</label>
      <input id="nome" class="form-control" type="text" name="nome"
        value="<?php if(isset($registro)) echo $registro['nome']; ?>" required>
    </div>
    <div class="from-group">
      <label for="funcao">Função</label>
      <input id="funcao" class="form-control" type="text" name="funcao"
        value="<?php if(isset($registro)) echo $registro['funcao']; ?>" required>
    </div>
    <div class="from-group">
      <label for="cpf">CPF</label>
      <input id="cpf" class="form-control" type="text" name="cpf"
        value="<?php if(isset($registro)) echo $registro['cpf']; ?>" required>
    </div>
    <div class="from-group">
      <label for="salario">Salário</label>
      <input id="salario" class="form-control" type="number" name="salario"
        value="<?php if(isset($registro)) echo $registro['salario']; ?>" required>
    </div>
    <br>
    <button class="btn btn-info" type="submit">Enviar</button>
  </form>
</div>