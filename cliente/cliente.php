
<?php

require_once '../config/conexao.php';
  
if(!isset($_GET['acao'])) $acao="listar";
    else $acao = $_GET['acao'];




   /**
    * Ação de listar
    */
    if($acao=="listar"){
        $sql   = "SELECT * FROM cliente";
        $query = $con->query($sql);
        $registros = $query->fetchAll();
 
        require_once '../template/cabecalho.php';
        require_once 'lista_cliente.php';
        require_once '../template/rodape.php';
     }

     /**
    * Ação Novo
    **/
    else if($acao == "novo"){
        require_once '../template/cabecalho.php';
        require_once 'form_cliente.php';
        require_once '../template/rodape.php';
      }

      /**
    * Ação Gravar
    **/
    else if($acao == "gravar"){
        $registro = $_POST;

        // var_dump($registro);
        $sql = "INSERT INTO cliente(nome, telefone, cpf)
        VALUES(:nome, :telefone, :cpf)";
        $query = $con->prepare($sql);
        $result = $query->execute($registro);
        if($result){
            header('Location: ./cliente.php');
        }else{
            echo "Erro ao tentar inserir o registro";
        }
    }

     /**
    * Ação Excluir
    **/
    else if($acao == "excluir"){
        $id    = $_GET['id'];
        $sql   = "DELETE FROM cliente WHERE id = :id";
        $query = $con->prepare($sql);

        $query->bindParam(':id', $id);

        $result = $query->execute();
        if($result){
            header('Location: ./cliente.php');
        }else{
            echo "Erro ao tentar remover o resgitro de id: " . $id;
        }
    }
    /**
    * Ação Buscar
    **/
    else if($acao == "buscar"){
        $id    = $_GET['id'];
        $sql   = "SELECT * FROM cliente WHERE id = :id";
        $query = $con->prepare($sql);
        $query->bindParam(':id', $id);

        $query->execute();
        $registro = $query->fetch();

        // var_dump($registro); exit;

        require_once '../template/cabecalho.php';
        require_once 'form_cliente.php';
        require_once '../template/rodape.php';

    }

    /**
    * Ação Atualizar
    **/
    else if($acao == "atualizar"){
        $sql   = "UPDATE cliente SET nome = :nome, telefone = :telefone,
                    cpf = :cpf WHERE id = :id";
        $query = $con->prepare($sql);

        $query->bindParam(':id', $_GET['id']);
        $query->bindParam(':nome', $_POST['nome']);
        $query->bindParam(':telefone', $_POST['telefone']);
        $query->bindParam(':cpf', $_POST['cpf']);
        $result = $query->execute();

        if($result){
            header('Location: ./cliente.php');
        }else{
            echo "Erro ao tentar atualizar os dados" . print_r($query->errorInfo());
        }
    }

?>