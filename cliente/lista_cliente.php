<?php


if (!isset($_SESSION['logado'])) {
  header('Location: login.php');
}
?>

<div class="container">
  <h2>Cliente</h2>
  <a class="btn btn-info" href="cliente.php?acao=novo">Novo</a>
  <a class="btn btn-info" href="gerarPDF.php">Relatorio</a>
  <?php if (count($registros)==0): ?>
    <p>Nenhum registro encontrado.</p>
  <?php else: ?>
    <table class="table table-hover table-stripped">
      <thead>
          <th>#</th>
          <th>Nome</th>
          <th>Telefone</th>
          <th>CPF</th>
          <th>Ações</th>
      </thead>
      <tbody>
        <?php foreach ($registros as $linha): ?>
          <tr>
            <td><?php echo $linha['id']; ?></td>
            <td><?php echo $linha['nome']; ?></td>
            <td><?php echo $linha['telefone']; ?></td>
            <td><?php echo $linha['cpf']; ?></td>
            <td>
                <a class="btn btn-warning btn-sm" href="cliente.php?acao=buscar&id=<?php echo $linha['id']; ?>">Editar</a>
                <a class="btn btn-danger btn-sm" href="cliente.php?acao=excluir&id=<?php echo $linha['id']; ?>">Excluir</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  <?php endif; ?>
</div>