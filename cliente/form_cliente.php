<?php

if (!isset($_SESSION['logado'])) {
  header('Location: login.php');
}
?>

<?php
    if(isset($registro)) $acao = "cliente.php?acao=atualizar&id=".$registro['id'];
    else $acao = "cliente.php?acao=gravar";
 ?>
<div class="container">
  <form class="" action="<?php echo $acao; ?>" method="post">
    <div class="from-group">
      <label for="nome">Nome</label>
      <input id="nome" class="form-control" type="text" name="nome"
        value="<?php if(isset($registro)) echo $registro['nome']; ?>" required>
    </div>
    <div class="from-group">
      <label for="telefone">Telefone</label>
      <input id="telefone" class="form-control" type="text" name="telefone"
        value="<?php if(isset($registro)) echo $registro['telefone']; ?>" required>
    </div>
    <div class="from-group">
      <label for="cpf">CPF</label>
      <input id="cpf" class="form-control" type="text" name="cpf"
        value="<?php if(isset($registro)) echo $registro['cpf']; ?>" required>
    </div>
    <br>
    <button class="btn btn-info" type="submit">Enviar</button>
  </form>
</div>

