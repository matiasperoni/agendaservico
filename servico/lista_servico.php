
<?php

if (!isset($_SESSION['logado'])) {
  header('Location: login.php');
}
?>

<div class="container print">
  <h2>Serviços</h2>
  <a class="btn btn-info" href="servico.php?acao=novo">Novo</a>
  <a class="btn btn-info" href="gerarPDF.php">Relatorio</a>  
  <?php if (count($registros)==0): ?>
    <p>Nenhum registro encontrado.</p>
  <?php else: ?>
    <table class="table table-hover table-stripped">
      <thead>
          <th>#</th>
          <th>Cliente</th>
          <th>Celular</th>
          <th>Funcionario</th>
          <th>Descrição</th>
          <th>Data</th>
          <th>Valor</th>
          <th>Ações</th>
      </thead>
      <tbody>
        <?php foreach ($registros as $linha): ?>
          <tr>
            <td><?= $linha['id']; ?></td>
            <td><?= $linha['cliente']; ?></td>
            <td><?= $linha['celular']; ?></td>
            <td><?= $linha['funcionario']; ?></td>
            <td><?= $linha['descricao']; ?></td>
            <td><?= $linha['dataServico']; ?></td>
            <td><?= $linha['valor']; ?></td>
            <td>
                <a class="btn btn-warning btn-sm" href="servico.php?acao=buscar&id=<?php echo $linha['id']; ?>">Editar</a>
                <a class="btn btn-danger btn-sm" href="servico.php?acao=excluir&id=<?php echo $linha['id']; ?>">Excluir</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  <?php endif; ?>
</div>
