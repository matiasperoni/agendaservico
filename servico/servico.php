<?php

    require_once '../config/conexao.php';
    
    

    if(!isset($_GET['acao'])) $acao="listar";
    else $acao = $_GET['acao'];

    /**
    * Ação de listar
    */
    if($acao=="listar"){
       $sql = "SELECT s.id, s.descricao, s.dataServico, s.valor, f.nome as funcionario, ce.nome as celular, c.nome as cliente
       FROM servico s 
       INNER JOIN cliente c ON s.id_cliente = c.id
       INNER JOIN funcionario f ON f.id = s.id_funcionario
       INNER JOIN celular ce ON ce.id = s.id_celular";
       
        $query = $con->query($sql);
        $registros = $query->fetchAll();  

       //print_r($registros); exit;
       require_once '../template/cabecalho.php';
       require_once 'lista_servico.php';
       require_once '../template/rodape.php';

    }

    /**
    * Ação Novo
    **/
    else if($acao == "novo"){
      $lista_cliente = getCliente();
      $lista_celular = getCelular();
      $lista_funcionario = getFuncionario();

      // print_r($lista_servico); exit;
      require_once '../template/cabecalho.php';
      require_once 'form_servico.php';
      require_once '../template/rodape.php';
    }
    /**
    * Ação Gravar
    **/
    
    else if($acao == "gravar"){
        $registro = $_POST;

        // var_dump($registro);

        $sql = "INSERT INTO servico(id_cliente, id_celular, id_funcionario, descricao, dataServico, valor)
                  VALUES(:id_cliente, :id_celular, :id_funcionario, :descricao, :dataServico, :valor)";
        $query = $con->prepare($sql);
        $result = $query->execute($registro);
        if($result){
            header('Location: ./servico.php');
        }else{
            echo "Erro ao tentar inserir o registro, msg: " . print_r($query->errorInfo());
        }
    }
    /**
    * Ação Excluir
    **/
    else if($acao == "excluir"){
        $id    = $_GET['id'];
        $sql   = "DELETE FROM servico WHERE id = :id";
        $query = $con->prepare($sql);

        $query->bindParam(':id', $id);

        $result = $query->execute();
        if($result){
            header('Location: ./servico.php');
        }else{
            echo "Erro ao tentar remover o resgitro de id: " . $id;
        }
    }
    /**
    * Ação buscar
    **/
    else if($acao == "buscar"){
        $id    = $_GET['id'];
        $sql   = "SELECT * FROM servico WHERE id = :id";
        $query = $con->prepare($sql);
        $query->bindParam(':id', $id);

        $query->execute();
        $registro = $query->fetch();

        // var_dump($registro); exit;
        $lista_cliente = getCliente();
        $lista_celular = getCelular();
        $lista_funcionario = getFuncionario();
        require_once '../template/cabecalho.php';
        require_once 'form_servico.php';
        require_once '../template/rodape.php';
    }
    /**
    * Ação Atualizar
    **/
    else if($acao == "atualizar"){
        $sql   = "UPDATE servico SET id_cliente = :id_cliente, id_celular = :id_celular,
                    id_funcionario = :id_funcionario, descricao = :descricao, 
                    dataServico = :dataServico, valor = :valor WHERE id = :id";
        $query = $con->prepare($sql);


        $query->bindParam(':id', $_GET['id']);
        $query->bindParam(':id_cliente', $_POST['id_cliente']);
        $query->bindParam(':id_celular', $_POST['id_celular']);
        $query->bindParam(':id_funcionario', $_POST['id_funcionario']);
        $query->bindParam(':descricao', $_POST['descricao']);
        $query->bindParam(':dataServico', $_POST['dataServico']);
        $query->bindParam(':valor', $_POST['valor']);
        $result = $query->execute();

        if($result){
            header('Location: ./servico.php');
        }else{
            echo "Erro ao tentar atualizar os dados" . print_r($query->errorInfo());
        }
    }
    

    //função que retorna a lista de gêneros cadastrados no banco
    function getCliente(){
        $sql   = "SELECT * FROM cliente";
        $query = $GLOBALS['con']->query($sql);
        $lista_cliente = $query->fetchAll();
        return $lista_cliente;
    }

    function getCelular(){
        $sql   = "SELECT * FROM celular";
        $query = $GLOBALS['con']->query($sql);
        $lista_celular = $query->fetchAll();
        return $lista_celular;
    }

    function getFuncionario(){
        $sql   = "SELECT * FROM funcionario";
        $query = $GLOBALS['con']->query($sql);
        $lista_funcionario = $query->fetchAll();
        return $lista_funcionario;
    }

    
 ?>
