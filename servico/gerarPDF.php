<?php
    define('FPDF_FONTPATH', 'font/');
    require_once 'C:\xampp\htdocs\agendaServico\fpdf/fpdf.php';
    require_once  'C:\xampp\htdocs\agendaServico\config\conexao.php';

    // para exibir todos os registrosuse esse select : $sql="SELECT * FROM users ";

    $sql = "SELECT s.id, s.descricao, s.dataServico, s.valor, f.nome as funcionario, ce.nome as celular, c.nome as cliente
       FROM servico s 
       INNER JOIN cliente c ON s.id_cliente = c.id
       INNER JOIN funcionario f ON f.id = s.id_funcionario
       INNER JOIN celular ce ON ce.id = s.id_celular";
       
        $query = $con->query($sql);
        $registros = $query->fetchAll();   
    
        
       
    
        $pdf = new FPDF('P','mm','A4');
    $pdf->AddPage();
    $pdf->SetFont('Arial','B',16);
    $pdf->Cell(190,10,utf8_decode('Relatório de Serviços'),0,0,"C");
    $pdf->Ln(15);
    
    $pdf->SetFont("Arial","I",10);
    $pdf->Cell(40,7,"Cliente",1,0,"C");
    $pdf->Cell(30,7,"Celular",1,0,"C");
    $pdf->Cell(30,7,"Funcionario",1,0,"C");
    $pdf->Cell(35,7,  utf8_decode("Descrição"),1,0,"C");
    $pdf->Cell(30,7, ("Data"),1,0,"C");
    $pdf->Cell(30,7,"Valor",1,0,"C");
    $pdf->Ln();
    
    foreach ($registros as $linha) {
        $pdf->Cell(40,7, utf8_decode($linha['cliente']),1,0,"C");
        $pdf->Cell(30,7, utf8_decode($linha['celular']),1,0,"C");
        $pdf->Cell(30,7, utf8_decode($linha['funcionario']),1,0,"C");
        $pdf->Cell(35,7,  utf8_decode($linha['descricao']),1,0,"C");
        $pdf->Cell(30,7,  $linha['dataServico'],1,0,"C");
        $pdf->Cell(30,7, $linha['valor'],1,0,"C");
            $pdf->Ln();
    }
    
    $pdf->Output();
    
    ?>