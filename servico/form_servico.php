<?php

if (!isset($_SESSION['logado'])) {
  header('Location: login.php');
}
?>

<?php
    if(isset($registro)) $acao = "servico.php?acao=atualizar&id=".$registro['id'];
    else $acao = "servico.php?acao=gravar";
 ?>
<div class="container">
  <form class="" action="<?php echo $acao; ?>" method="post">
  <div class="from-group">
      <label for="id_cliente">Cliente</label>
      <select class="form-control" name="id_cliente" required>
        <option value="">Escolha um item na lista</option>
        <?php foreach ($lista_cliente as $item): ?>
          <option value="<?php echo $item['id']; ?>"
            <?php if(isset($registro) && $registro['id_cliente']==$item['id']) echo "selected";?>>
            <?php echo $item['nome']; ?>
          </option>
        <?php endforeach; ?>
      </select>
    </div>
    <div class="from-group">
      <label for="id_celular">Celular</label>
      <select class="form-control" name="id_celular" required>
        <option value="">Escolha um item na lista</option>
        <?php foreach ($lista_celular as $item): ?>
          <option value="<?php echo $item['id']; ?>"
            <?php if(isset($registro) && $registro['id_celular']==$item['id']) echo "selected";?>>
            <?php echo $item['nome']; ?>
          </option>
        <?php endforeach; ?>
      </select>
    </div>
    <div class="from-group">
      <label for="id_funcionario">Funcionario</label>
      <select class="form-control" name="id_funcionario" required>
        <option value="">Escolha um item na lista</option>
        <?php foreach ($lista_funcionario as $item): ?>
          <option value="<?php echo $item['id']; ?>"
            <?php if(isset($registro) && $registro['id_funcionario']==$item['id']) echo "selected";?>>
            <?php echo $item['nome']; ?>
          </option>
        <?php endforeach; ?>
      </select>
    </div>
    <div class="from-group">
      <label for="descricao">Descrição</label>
      <input id="descricao" class="form-control" type="text" name="descricao"
        value="<?php if(isset($registro)) echo $registro['descricao']; ?>" required>
    </div>
    <div class="from-group">
      <label for="dataServico">Data</label>
      <input id="dataServico" class="form-control" type="date" name="dataServico"
        value="<?php if(isset($registro)) echo $registro['dataServico']; ?>" required>
    </div>
    <div class="from-group">
      <label for="valor">Valor</label>
      <input id="valor" class="form-control" type="decimal" name="valor"
        value="<?php if(isset($registro)) echo $registro['valor']; ?>" required>
    </div>
    <br>
    <button class="btn btn-info" type="submit">Enviar</button>
  </form>
</div>

