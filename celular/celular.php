<?php
    require_once '../config/conexao.php';

    

    if(!isset($_GET['acao'])) $acao="listar";
    else $acao = $_GET['acao'];

    /**
    * Ação de listar
    */
    if($acao=="listar"){
       $sql   = "SELECT c.id, c.nome, c.cor, m.nome as marca
                    FROM celular c
                    INNER JOIN marca m ON m.id=c.id_marca";
       $query = $con->query($sql);
       $registros = $query->fetchAll();

       // print_r($registros); exit;
       require_once '../template/cabecalho.php';
       require_once 'lista_celular.php';
       require_once '../template/rodape.php';
    }
    /**
    * Ação Novo
    **/
    else if($acao == "novo"){
      $lista_marca = getMarca();

      // print_r($lista_celular); exit;
      require_once '../template/cabecalho.php';
      require_once 'form_celular.php';
      require_once '../template/rodape.php';
    }
    /**
    * Ação Gravar
    **/
    else if($acao == "gravar"){
        $registro = $_POST;

        // var_dump($registro);

        $sql = "INSERT INTO celular(nome, cor, id_marca)
                  VALUES(:nome, :cor, :id_marca)";
        $query = $con->prepare($sql);
        $result = $query->execute($registro);
        if($result){
            header('Location: ./celular.php');
        }else{
            echo "Erro ao tentar inserir o registro, msg: " . print_r($query->errorInfo());
        }
    }
    /**
    * Ação Excluir
    **/
    else if($acao == "excluir"){
        $id    = $_GET['id'];
        $sql   = "DELETE FROM celular WHERE id = :id";
        $query = $con->prepare($sql);

        $query->bindParam(':id', $id);

        $result = $query->execute();
        if($result){
            header('Location: ./celular.php');
        }else{
            echo "Erro ao tentar remover o resgitro de id: " . $id;
        }
    }
    /**
    * Ação buscar
    **/
    else if($acao == "buscar"){
        $id    = $_GET['id'];
        $sql   = "SELECT * FROM celular WHERE id = :id";
        $query = $con->prepare($sql);
        $query->bindParam(':id', $id);

        $query->execute();
        $registro = $query->fetch();

        // var_dump($registro); exit;
        $lista_marca = getMarca();
        require_once '../template/cabecalho.php';
        require_once 'form_celular.php';
        require_once '../template/rodape.php';
    }
    /**
    * Ação Atualizar
    **/
    else if($acao == "atualizar"){
        $sql   = "UPDATE celular SET nome = :nome, cor = :cor,
                    id_marca = :id_marca WHERE id = :id";
        $query = $con->prepare($sql);


        $query->bindParam(':id', $_GET['id']);
        $query->bindParam(':nome', $_POST['nome']);
        $query->bindParam(':cor', $_POST['cor']);
        $query->bindParam(':id_marca', $_POST['id_marca']);
        $result = $query->execute();

        if($result){
            header('Location: ./celular.php');
        }else{
            echo "Erro ao tentar atualizar os dados" . print_r($query->errorInfo());
        }
    }

    //função que retorna a lista de marcas cadastrados no banco
    function getMarca(){
        $sql   = "SELECT * FROM marca";
        $query = $GLOBALS['con']->query($sql);
        $lista_marca = $query->fetchAll();
        return $lista_marca;
    }
 ?>
