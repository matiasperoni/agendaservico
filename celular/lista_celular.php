<?php

if (!isset($_SESSION['logado'])) {
  header('Location: login.php');
}
?>

<div class="container print">
  <h2>Celulares</h2>
  <a class="btn btn-info" href="celular.php?acao=novo">Novo</a>  
  <?php if (count($registros)==0): ?>
    <p>Nenhum registro encontrado.</p>
  <?php else: ?>
    <table class="table table-hover table-stripped">
      <thead>
          <th>#</th>
          <th>Nome</th>
          <th>cor</th>
          <th>Marca</th>
          <th>Ações</th>
      </thead>
      <tbody>
        <?php foreach ($registros as $linha): ?>
          <tr>
            <td><?= $linha['id']; ?></td>
            <td><?= $linha['nome']; ?></td>
            <td><?= $linha['cor']; ?></td>
            <td><?= $linha['marca']; ?></td>
            <td>
                <a class="btn btn-warning btn-sm" href="celular.php?acao=buscar&id=<?php echo $linha['id']; ?>">Editar</a>
                <a class="btn btn-danger btn-sm" href="celular.php?acao=excluir&id=<?php echo $linha['id']; ?>">Excluir</a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  <?php endif; ?>
</div>
