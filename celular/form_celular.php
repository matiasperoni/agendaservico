<?php

if (!isset($_SESSION['logado'])) {
  header('Location: login.php');
}
?>

<?php
    if(isset($registro)) $acao = "celular.php?acao=atualizar&id=".$registro['id'];
    else $acao = "celular.php?acao=gravar";
 ?>
<div class="container">
  <form class="" action="<?php echo $acao; ?>" method="post">
    <div class="from-group">
      <label for="nome">Nome</label>
      <input id="nome" class="form-control" type="text" name="nome"
        value="<?php if(isset($registro)) echo $registro['nome']; ?>" required>
    </div>
    <div class="from-group">
      <label for="cor">Cor</label>
      <input id="cor" class="form-control" type="text" name="cor"
        value="<?php if(isset($registro)) echo $registro['cor']; ?>" required>
    </div>
    <div class="from-group">
      <label for="id_marca">Marca</label>
      <select class="form-control" name="id_marca" required>
        <option value="">Escolha um item na lista</option>
        <?php foreach ($lista_marca as $item): ?>
          <option value="<?php echo $item['id']; ?>"
            <?php if(isset($registro) && $registro['id_marca']==$item['id']) echo "selected";?>>
            <?php echo $item['nome']; ?>
          </option>
        <?php endforeach; ?>
      </select>
    </div>
    <br>
    <button class="btn btn-info" type="submit">Enviar</button>
  </form>
</div>