

<?php require_once 'template/cabecalho.php'; ?>

<div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
      <div class="col-md-5 p-lg-5 mx-auto my-5">
        <h1 class="display-4 font-weight-normal">Tech Cell</h1>
        <p class="lead font-weight-normal">Temos uma equipe Técnica preparada e competente com vasta experiencia no mercado eletrônico, nosso laboratório é preparado com os melhores equipamentos, para o reparo do seu aparelho.</p>
        
      </div>
      <div class="product-device box-shadow d-none d-md-block"></div>
      <div class="product-device product-device-2 box-shadow d-none d-md-block"></div>
    </div>

    <div class="d-md-flex flex-md-equal w-100 my-md-3 pl-md-3">
      <div class="bg-dark mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center text-white overflow-hidden">
        <div class="my-3 py-3">
          <h2 class="display-5">CELULAR MOLHADO?</h2>
          <p class="lead">Realizaremos o banho químico, tirando todas as impurezas e possíveis oxidações que tem ou possa ter. Não tente ligar o celular, retire a bateria e venha o mais rápido possível para o Novo Smart</p>
        </div>
        <div class="bg-light box-shadow mx-auto" style="width: 80%; height: 300px; border-radius: 21px 21px 0 0;"></div>
      </div>
      <div class="bg-light mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden">
        <div class="my-3 p-3">
          <h2 class="display-5">BATERIA ESTUFADA OU DESCARREGANDO RÁPIDO</h2>
          <p class="lead">Isso pode significar que a vida útil de sua bateria está no fim, venha até nós enquanto ainda está funcionando.</p>
        </div>
        <div class="bg-dark box-shadow mx-auto" style="width: 80%; height: 300px; border-radius: 21px 21px 0 0;"></div>
      </div>
    </div>

    <div class="d-md-flex flex-md-equal w-100 my-md-3 pl-md-3">
      <div class="bg-light mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden">
        <div class="my-3 p-3">
          <h2 class="display-5">TELA QUEBRADA</h2>
          <p class="lead">Além de sempre usarmos peças de qualidade nas trocas, temos o serviço de troca de vidro onde você tem economia de até 70%. Venha com seu aparelho para avaliarmos.</p>
        </div>
        <div class="bg-dark box-shadow mx-auto" style="width: 80%; height: 300px; border-radius: 21px 21px 0 0;"></div>
      </div>
      <div class="bg-primary mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center text-white overflow-hidden">
        <div class="my-3 py-3">
          <h2 class="display-5">NÃO CARREGA</h2>
          <p class="lead">Se seu conector de carga está com problema, venha com seu aparelho para avaliarmos e realizar a troca caso seja necessário.</p>
        </div>
        <div class="bg-light box-shadow mx-auto" style="width: 80%; height: 300px; border-radius: 21px 21px 0 0;"></div>
      </div>
    </div>

    

<?php

require_once 'template/rodape.php';

?>